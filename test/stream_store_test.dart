import 'src/effect_test.dart' as effect_test;
import 'src/reducer_test.dart' as reducer_test;
import 'src/store_test.dart' as store_test;
import 'src/logging_transformer_test.dart' as logging_transformer_test;

void main() {
  effect_test.main();
  reducer_test.main();
  store_test.main();
  logging_transformer_test.main();
}
